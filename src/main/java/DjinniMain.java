import provider.DjinniProvider;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DjinniMain {

    public static void main(String[] args) {
        DjinniProvider djinn = new DjinniProvider();

        try {
            List<String> categories = new LinkedList<>();
            categories.add("QA+Automation");
            categories.add("C%2B%2B");
            categories.add("Java");
            categories.add("Design");
            categories.add("Python");
            categories.add("PHP");
            categories.add("QA");

            Set<String> links = new HashSet<>();
            categories.forEach(cat -> {
                try {
                    links.addAll(djinn.getLinks("https://djinni.co/jobs/", cat));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            for (String link : links) {
                djinn.getVacancy(link);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
