import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import provider.DouProvider;
import provider.NoSuchHeaderException;

import java.io.IOException;
import java.util.List;


public class DouMain {
    public static void main(String[] args) throws IOException {
        DouProvider provider = new DouProvider();

        // получили страницу вакансий по ссылке
        String url = "https://jobs.dou.ua/vacancies/?category=Java&exp=0-1";
        List<String> vacanciesUrls = provider.getLinks(url);

        vacanciesUrls.forEach(u -> {
            try {
                System.out.println(u);
                Document doc = Jsoup.connect(u).get();
                List<String> requirements = provider.getSkillsInParagraph(doc, "Необходимые навыки");
                System.out.println("Необходимые навыки");
                requirements.forEach(System.out::println);
                List<String> additional = provider.getSkillsInParagraph(doc, "Будет плюсом");
                System.out.println("Будет плюсом");
                additional.forEach(System.out::println);
                System.out.println("\n");
            } catch (IOException e) {
                System.out.println("urls seems like an invalid");
            } catch (NoSuchHeaderException e){
                System.out.println("\n" + e.getMessage());
            }
        });
    }
}
