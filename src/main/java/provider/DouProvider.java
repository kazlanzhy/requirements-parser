package provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DouProvider {

    public List<String> getLinks(String searchUrl) throws IOException {
        // получили страницу вакансий по ссылке поиска
        Document doc = Jsoup.connect(searchUrl).get();
        List<String> links = new LinkedList<>();

        // записали ссылки с вакансиями в список
        doc.getElementsByClass("vt")
                .select("a[href]")
                .forEach(element -> links.add(element.attr("href")));

        return links;
    }

    public List<String> getSkillsInParagraph(Document document, String header) throws NoSuchHeaderException {
        if (!hasHeader(document, header)) {
            throw new NoSuchHeaderException(header + " NOT_FOUND");
        }

        int paragraphNumber = getHeadersInVacancySection(document).indexOf(header);
        Elements elements = document.getElementsByClass("text b-typo vacancy-section").select("p");
        Element paragraph = elements.get(paragraphNumber);
        //у кого есть предложение как лучше парсить контент в параграфе - welcome
        String filtered = paragraph.toString()
                .replaceAll("[^a-zA-Z0-9а-яА-Я <>&/.,()]", "")
                .replaceAll("&nbsp", " ")
                .replaceAll("</p>", "")
                .replaceAll("<p>", "");

        List<String> skills = new LinkedList<>();
        Collections.addAll(skills, filtered.split("<br>"));

        return skills.stream()
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private boolean hasHeader(Document document, String header) {
        return getHeadersInVacancySection(document).stream()
                .anyMatch(header::equalsIgnoreCase);
    }

    private List<String> getHeadersInVacancySection(Document document) {
        return document.getElementsByClass("g-h3").stream()
                .map(Element::text)
                .collect(Collectors.toList());
    }
}

