package provider;

import java.io.IOException;
import java.util.LinkedList;

/**
 * черновой интерфейс
 */
public interface ProviderInterface {


    /*
     * getLinks.forEach(item -> {
     *      return getVacancy(item);
     * })
     */
    Object provide();

    LinkedList<String> getLinks(String url) throws IOException;

    LinkedList getVacancy(String url) throws IOException;
}
