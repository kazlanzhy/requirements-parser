package provider;

public class NoSuchHeaderException extends Exception {
    public NoSuchHeaderException() {
        super();
    }

    public NoSuchHeaderException(String message) {
        super(message);
    }
}
