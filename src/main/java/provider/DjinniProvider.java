package provider;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DjinniProvider {


    public void provide() {
        try {
            List<String> categories = new LinkedList<>();
            categories.add("QA+Automation");
            categories.add("C%2B%2B");
            categories.add("Java");
            categories.add("Design");
            categories.add("Python");
            categories.add("PHP");
            categories.add("QA");

            Set<String> links = new HashSet<>();
            categories.forEach(cat -> {
                try {
                    links.addAll(getLinks("https://djinni.co/jobs/", cat));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            for (String link : links) {
                getVacancy(link);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Set<String> getLinks(String url, String category) throws IOException {
        Set<String> links = new HashSet<>();

        int page = 1;
        while(page < 2) {
            Connection conn = Jsoup.connect(String.format("%s?page=%d&primary_keyword=%s", url, page++, category));

            /*
             * djinni отдаёт 500, если вакансий по page не найдены
             */
            if (conn.execute().statusCode() == 500) {
                break;
            }
            Document document = conn.get();
            document
                .select("a.profile")
                .forEach(link -> {
                    String linkHref = link.attr("href");
                    String vacancyUrl = getDomain().concat(linkHref);
                    links.add(vacancyUrl);
                    System.out.println(vacancyUrl);
                });
        }

        return links;
    }

    public List<String> getVacancy(String url) throws IOException {
        LinkedList<String> blocks = new LinkedList<>();

        Document document = Jsoup.connect(url).get();

        System.out.println(getVacancyTitle(document));

        Elements elements = document
                .select(".profile-page-section")
                .not(".text-small");
        /*
         * Все требования в большинстве находятся в первом блоке
         */
        int requirementNumber = elements.size() > 1 ? 1 : 0;

        Element requirement = elements.get(requirementNumber);
        String html = requirement.html();
        String[] reqsList = html.split("<br>\\s*?<br>");
        Document reqsDocument = Jsoup.parse(reqsList[0], "", Parser.htmlParser());
        reqsDocument
                .body()
                .textNodes()
                .forEach(node -> {
                    System.out.println(node.text());
                });

        return blocks;
    }

    private String getVacancyTitle(Document document) {
        return document
                .select(".page-header h1")
                .text();
    }

    public String getDomain() {
        return "https://djinni.co";
    }

    public String getUrl() {
        return "https://djinni.co/jobs";
    }
}

// blocks.add(textNode.text());
// System.out.println(textNode.text());

